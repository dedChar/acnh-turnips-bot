import re
import math
import traceback
from asyncio import TimeoutError
from enum import Enum

from discord import Message, DMChannel, Forbidden, NotFound, HTTPException, Guild, TextChannel, Embed, Game, Status, User, Reaction
from discord.ext.commands import dm_only, PrivateMessageOnly, guild_only, NoPrivateMessage, has_permissions

from peewee import *
from playhouse.migrate import *
from datetime import date, datetime, timedelta
from pytz import UTC

from utils import conf, log, schedule
from utils.storage import getDatabase, set_version, get_version, get_version_str
from utils.predict_prices import Predictor, get_surefire_prediction, get_index_of_max_prediction, get_index_of_min_prediction, InvalidPrices
from utils.migrations import pattern_migration
from utils.message import ReactableMessage

CONF = conf.get()
LOG = log.get('turnips')
DB = getDatabase('turnips')

FRUIT = "🍎🍐🍑🍒🍊"
EDITING_USERS = []

class Person(Model):
    id = IntegerField(primary_key=True)
    nativeFruit = CharField(null=True, max_length=1)
    islandName = CharField(null=True)
    ingameName = CharField(null=True)
    current_pattern = IntegerField(default=-1)
    last_pattern = IntegerField(default=-1)
    pattern_valid_until = DateField(default=datetime.now)

    class Meta:
        database = DB


class TurnipPrice(Model):
    turnipPrice = IntegerField(null=False)
    date = DateField(null=False)
    morning = BooleanField(null=False)
    person = ForeignKeyField(Person, backref="turnips", null=False)

    class Meta:
        database = DB
        primary_key = CompositeKey('date', 'person', 'morning')


class TurnipMessage(Model):
    guildId = IntegerField(null=False)
    channelId = IntegerField(null=False)
    messageId = IntegerField(null=False)

    class Meta:
        database = DB
        primary_key = CompositeKey('guildId', 'channelId')


if get_version_str(DB) == '0.0.0':
    # initialize database
    DB.connect()
    DB.create_tables([Person, TurnipPrice, TurnipMessage])
    DB.close()
    set_version(DB, '1.0.0', "Initial Version")
if get_version_str(DB) == '1.0.0':
    # apply pattern migration
    DB.connect()
    pattern_migration(DB)
    DB.close()
    set_version(DB, '1.1.0', "Added prediction patterns")


def getDatetime() -> datetime:
    return datetime.now(CONF["timezone"])


def getToday(dt: datetime = None) -> date:
    dt = getDatetime() if dt is None else dt
    return dt.date()


def isMorning(dt: datetime = None) -> bool:
    dt = getDatetime() if dt is None else dt
    return dt.hour < 12

def isSunday(dt: datetime = None) -> bool:
    dt = getDatetime() if dt is None else dt
    return dt.isoweekday() == 7

def isMarketClosed(dt: datetime = None) -> bool:
    dt = getDatetime() if dt is None else dt
    return dt.hour < 8 or dt.hour >= 22 or (isSunday(dt) and dt.hour >= 12)

def getStartEndOfWeek(dt: datetime = None) -> (date, date):
    dt = getDatetime() if dt is None else dt
    start = dt - timedelta(days=(dt.weekday() + 1) % 7)
    end = start + timedelta(days=6)
    return (start.date(), end.date())


def getStartOfNextWeek(dt: datetime = None) -> date:
    dt = getDatetime() if dt is None else dt
    return (dt - timedelta(days=(dt.weekday() + 1) % 7) + timedelta(weeks=1)).date()


def daterange(start_date: datetime, end_date: datetime):
    for n in range(int((end_date - start_date).days) + 1):
        yield start_date + timedelta(n)

SCHEDULED_PRESENCE = None
async def updatePresence(bot):
    """Update discord presence based on current market time."""
    # update presence to idle if market is closed
    if isMarketClosed(getDatetime()):
        LOG.debug("changing status to idle")
        await bot.change_presence(activity=None, status=Status.idle)
    else:
        LOG.debug("changing status to online")
        await bot.change_presence(activity=Game("Animal Crossing: New Horizons"), status=Status.online)

    SCHEDULED_PRESENCE = schedule.add(bot, updatePresence, 1800, absolute=False, args=[bot])

async def sendMessage(channel: TextChannel) -> Message:
    # send the message
    msg = await channel.send(embed=buildEmbed(channel))

    # save the message id
    turnipMsg = getMessage(channel.guild.id, channel.id)
    if turnipMsg is None:
        turnipMsg = TurnipMessage.create(
            guildId=channel.guild.id, channelId=channel.id, messageId=msg.id)
    else:
        turnipMsg.messageId = msg.id
        turnipMsg.save()

    # try to pin the message
    try:
        await msg.pin()
    except Forbidden:
        LOG.error(
            "Missing pinning permission on guild#{0.guild.id} '{0.guild.name}' in channel#{0.id} '{0.name}!'".format(channel))
    except NotFound:
        LOG.critical(
            "Could not find the market message that was just sent on guild#{0.guild.id} '{0.guild.name}' in channel#{0.id} '{0.name}!".format(channel))
        # turnipMsg.delete_instance()
    except HTTPException as e:
        LOG.critical("Failed to pin message: " + e)

    return msg


async def updateMessages(bot, guilds, forceNew):
    """Update all messages"""
    query = TurnipMessage.select()

    # select only certain gPartialEmoji(name=uilds
    if guilds:
        query = query.where(TurnipMessage.guildId << guilds)

    for turnipMsg in query:
        channel = bot.get_channel(turnipMsg.channelId)
        if channel is not None:
            try:
                msg = await channel.fetch_message(turnipMsg.messageId)
            except Forbidden:
                # send a new message if permission was lost
                LOG.critical(
                    "Forbidden to fetch message in channel#{0.id} '{0.name}'!".format(channel))
                await sendMessage(channel)
                continue
            except NotFound:
                # send a new message if old one was deleted
                LOG.critical(
                    "Message in channel#{0.id} '{0.name}' was lost!".format(channel))
                await sendMessage(channel)
                continue
            except HTTPException:
                LOG.critical(
                    "a HTTP error occured trying to get message in channel#{0.id} '{0.name}'".format(channel))
                continue

            if forceNew:
                # try to delete old one
                try:
                    await msg.delete()
                except (Forbidden, NotFound, HTTPException):
                    pass

                # send new
                await sendMessage(channel)
            else:
                # update the old message
                try:
                    await msg.edit(embed=buildEmbed(channel))
                except HTTPException:
                    LOG.critical(
                        "a HTTP error occured trying to edit message in channel#{0.id} '{0.name}'".format(channel))

    # schedule the next update
    scheduleNextUpdate(bot)

SCHEDULED_UPDATE = None


def scheduleNextUpdate(bot):
    """Schedule message updates for next market change time"""
    global SCHEDULED_UPDATE

    marketChange = False
    # get the next time for update
    dt = getDatetime()
    if dt.hour < 8:
        dt = dt.replace(hour=8)
        marketChange = True
    elif dt.hour < 12:
        dt = dt.replace(hour=12)
        marketChange = True
    elif dt.hour < 22:
        dt = dt.replace(hour=22)
    else:
        marketChange = True
        dt = dt.replace(hour=8)
        dt += timedelta(days=1)

    dt = dt.replace(minute=0, second=0, microsecond=50)

    # cancel last scheduled update
    if SCHEDULED_UPDATE is not None:
        LOG.debug("canceling old update")
        schedule.cancel(SCHEDULED_UPDATE)

    LOG.debug("scheduling next update at {}".format(dt.timestamp()))
    SCHEDULED_UPDATE = schedule.add(
        bot, updateMessages, dt.timestamp(), args=[bot, [], marketChange])


def hasPerson(id: int) -> bool:
    try:
        Person.get_by_id(id)
        return True
    except DoesNotExist:
        return False


def getPerson(id: int) -> Person:
    # try to get the person
    try:
        return Person.get_by_id(id)
    except DoesNotExist:
        # create a new Person row
        person = Person.create(id=id)
        return person


def getTurnipPrice(person: Person, date: date = None, morning: bool = None) -> TurnipPrice:
    # use current date
    dt = getDatetime()
    if date is None:
        date = getToday(dt)
    if morning is None:
        morning = isMorning(dt)

    try:
        return TurnipPrice.get((TurnipPrice.date == date) & (TurnipPrice.person == person) & (TurnipPrice.morning == morning))
    except DoesNotExist:
        return None


def setTurnipPrice(price: int, person: Person, date: date = None, morning: bool = None):
    # use current date
    dt = getDatetime()
    if date is None:
        date = getToday(dt)
    if morning is None:
        morning = isMorning(dt)

    # check if turnip price was set today
    tp = getTurnipPrice(person, date, morning)
    if tp is not None:
        tp.turnipPrice = price
        tp.save()
    else:
        # create a new entry for today
        TurnipPrice.create(turnipPrice=price, date=date,
                           person=person, morning=morning)


def getMessage(guild: int, channel: int) -> TurnipMessage:
    try:
        return TurnipMessage.get((TurnipMessage.guildId == guild) & (TurnipMessage.channelId == channel))
    except DoesNotExist:
        return None


def registerMessage(guild: int, channel: int, message: int) -> TurnipMessage:
    return TurnipMessage.create(guildId=guild, channelId=channel, messageId=message)


PERSON_TURNIP_ENTRY = "{4} <@{0.id}> ({0.ingameName}) on **{0.islandName}** {0.nativeFruit}: {1.turnipPrice} {2.value} {3} {4}"
THUMB_NORMAL = "https://static.dedchar.net/images/acnh/items/turnip.png"
THUMB_ROTTEN = "https://static.dedchar.net/images/acnh/items/turnip_rotten.png"
TITLE = "Stalk Market {0}"
MORNING_TIME = "(8AM - 12PM)"
NOON_TIME = "(12PM - 10PM)"
CLOSED_TIME = "(CLOSED)"
CLOSED_MESSAGE = "The stalk market is currently **closed**."
SUNDAY_MESSAGE = "Today is **turnip day**, go buy some! Turnips cannot be sold on sundays."
NO_ENTRIES_MESSAGE = "No prices reported yet!"
GENERIC_MESSAGE = "These are the current turnip prices of some server islanders:"


class PriceChange(Enum):
    RISING = '↗️'
    UNCHANGED = '➡️'
    FALLING = '↘️'


def buildEmbed(channel: TextChannel) -> Embed:
    # current datetime to use for all functions
    dt = getDatetime()

    # get all members of the channel and select registered accounts
    memberIds = [m.id for m in channel.members]
    priceQuery = TurnipPrice.select()\
                    .join(Person, JOIN.INNER)\
                    .where((TurnipPrice.date == getToday(dt))
                            & (TurnipPrice.morning == isMorning(dt))
                            & (Person.id << memberIds))

    # create the embed with title and thumbnail
    embed = Embed(
        title=TITLE.format(CLOSED_TIME if isMarketClosed(
            dt) else MORNING_TIME if isMorning(dt) else NOON_TIME),
        color=0xFFAB40,
        timestamp=dt  # set timestamp to when this embed is built
    )

    # display rotten turnip on sundays
    embed.set_thumbnail(url=THUMB_ROTTEN if isSunday(dt) else THUMB_NORMAL)

    # build the description
    if isMarketClosed(dt):
        embed.description = CLOSED_MESSAGE
    else:
        desc = ""
        if isSunday(dt):
            priceQuery = priceQuery.order_by(+TurnipPrice.turnipPrice)
            desc = SUNDAY_MESSAGE + '\n\n'
        else:
            priceQuery = priceQuery.order_by(-TurnipPrice.turnipPrice)
            desc = GENERIC_MESSAGE + '\n\n'

        entriesAdded = False
        for price in priceQuery:
            person = price.person

            # add an award icon to highest price
            award = "🏆" if price == priceQuery[0] else ""

            # add sparkles depending on price
            sparkles = ""
            if price.turnipPrice >= 900:
                sparkles = "✨✨✨"
            elif price.turnipPrice >= 600:
                sparkles = "✨✨"
            elif price.turnipPrice >= 300:
                sparkles = "✨"

            # get the latest two prices
            lastPriceQuery = TurnipPrice.select().where(TurnipPrice.person ==
                                                person).order_by(-TurnipPrice.date, +TurnipPrice.morning).limit(2)

            if len(lastPriceQuery) == 0:
                LOG.debug(
                    "no turnip prices found for {0.ingameName} while building embed".format(person))
                continue

            # user has not submitted a turnip price for the current market time
            # this shouldnt happen anymore
            if lastPriceQuery[0].date != getToday(dt) or lastPriceQuery[0].morning != isMorning(dt):
                LOG.debug(
                    "latest turnip price for {0.ingameName} is too old".format(person))
                continue

            # get the price change arrow
            change = PriceChange.UNCHANGED
            if len(lastPriceQuery) == 2:
                delta = lastPriceQuery[0].turnipPrice - \
                    lastPriceQuery[1].turnipPrice
                if delta > 0:
                    change = PriceChange.RISING
                elif delta < 0:
                    change = PriceChange.FALLING

            # add the line
            desc += PERSON_TURNIP_ENTRY.format(person,
                                               price, change, award, sparkles) + '\n'
            entriesAdded = True

        # add no entires message if no one updated their price yet
        if not entriesAdded:
            desc += NO_ENTRIES_MESSAGE

        embed.description = desc

    return embed


class NoPricesRegistered(Exception):
    pass


def get_prediction_for_person(person: Person):
    if not person:
        raise ValueError("Person can't be None")
    current_week = getStartEndOfWeek()

    prices = []
    no_prices = True
    for weekday in daterange(current_week[0], current_week[1]):
        am_price = getTurnipPrice(person, weekday, True)
        pm_price = getTurnipPrice(person, weekday, False)

        no_prices = no_prices and am_price is None and pm_price is None

        # use NaN instead of None, because of the use of math.isnan in prediction implementation
        am_price = am_price.turnipPrice if am_price is not None else math.nan
        pm_price = pm_price.turnipPrice if pm_price is not None else math.nan

        prices.extend([am_price, pm_price])

    if no_prices:
        raise NoPricesRegistered()

    predictor = Predictor(prices, False, person.last_pattern)
    return predictor.analyze_possibilities()


def setup(bot):
    @bot.group()
    async def turnips(ctx):
        """Commands for using the stalk market (animal crossing turnip prices) tracker."""
        pass

    @turnips.command(name='predict')
    @dm_only()
    async def turnips_predict(ctx):
        """Make a prediction of this weeks prices based on the prices submitted thus far."""
        if not hasPerson(ctx.message.author.id):
            await ctx.channel.send("You are not registered yet! Please use `{0}turnips register \"Ingame Name\" \"Island Name\" 🍎/🍐/🍑/🍒/🍊` in my DMs".format(CONF["cmd_prefix"]))
            return

        person = getPerson(ctx.message.author.id)

        sunday, _ = getStartEndOfWeek()
        try:
            TurnipPrice.get((TurnipPrice.person == person) & (TurnipPrice.date == sunday) & (TurnipPrice.morning == True))
        except DoesNotExist:
            await ctx.channel.send("You did not enter your sunday morning tunip price! Predictions would be too inaccurate.")
            return

        # calculate prediction in background asyncly
        async def runPrediction(bot, channel: TextChannel, user: User, person: Person):
            LOG.debug("running prediction generator for {0.name} ({0.id})".format(user))
            # send prepare message    
            msg = await channel.send("<@{0.id}> Preparing your prediction...".format(person))
            await channel.trigger_typing()

            try:
                predicted_prices, all_patterns = get_prediction_for_person(person)
            except NoPricesRegistered:
                LOG.debug("no prices found for {0.name} ({0.id})".format(user))
                await channel.send("You have not registered any prices this week yet. Cannot create a prediction based on nothing!")
                return
            except InvalidPrices:
                LOG.warning("invalid prices found for {0.name} ({0.id})".format(user))
                await channel.send("Could not find a known pattern matching your reported turnip prices.")
                return
            except Exception as e:
                LOG.critical("Something really bad happened while running predicitons for {0.name} ({0.id})".format(user))
                LOG.critical(traceback.format_exc())
                await channel.send("Something went wrong while trying to predict your prices, please try again later.")
                return


            predict_embed = Embed(title="Turnip Predictions",
                                  description="These values are only estimations! Take with care!\nTimes that are worth looking out for are marked with ✨",
                                  color=0xFFAB40,
                                  timestamp = getDatetime())
            predict_embed.set_thumbnail(url=THUMB_NORMAL)
            predict_embed.set_footer(text="Prediction for {0.display_name} ({1.ingameName})".format(user, person), icon_url=str(user.avatar_url))

            day_names = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
            counter_price = 2
            for i in range(0, len(day_names)):
                day_name = day_names[i]
                minmax_am = all_patterns['prices'][counter_price]
                counter_price += 1
                minmax_pm = all_patterns['prices'][counter_price]
                counter_price += 1
                sparkle_am = minmax_am['min'] == all_patterns['week_guaranteed_minimum'] or minmax_am['max'] == all_patterns['week_max']
                sparkle_pm = minmax_pm['min'] == all_patterns['week_guaranteed_minimum'] or minmax_pm['max'] == all_patterns['week_max']

                value_am = "{0} - {1}".format(minmax_am['min'], minmax_am['max']) if minmax_am['min'] != minmax_am['max'] else "{0}".format(minmax_am['max'])
                value_pm = "{0} - {1}".format(minmax_pm['min'], minmax_pm['max']) if minmax_pm['min'] != minmax_pm['max'] else "{0}".format(minmax_pm['max'])

                predict_embed.add_field(name="{0} (am) {1}".format(day_name, "✨" if sparkle_am else ""), value=value_am)
                predict_embed.add_field(name="{0} (pm) {1}".format(day_name, "✨" if sparkle_pm else ""), value=value_pm)

            await msg.edit(content="<@{0.id}>".format(person), embed=predict_embed)
            LOG.debug("sent predictions for {0.name} ({0.id})".format(user))

        # run asyncly
        schedule.now(bot, runPrediction, [bot, ctx.channel, ctx.message.author, person])

    @turnips.command(name='edit')
    @dm_only()
    async def turnips_edit(ctx):
        """Change your past prices of this week."""
        user = ctx.message.author
        if hasPerson(user.id):
            person = getPerson(user.id)
        else:
            await ctx.channel.send("You are not registered yet! Please use `{0}turnips register \"Ingame Name\" \"Island Name\" 🍎/🍐/🍑/🍒/🍊` in my DMs".format(CONF["cmd_prefix"]))
            return
        
        if user in EDITING_USERS:
            await ctx.channel.send("You are already editing your turnip prices!")
            return

        # lock user out from updating current price slot
        EDITING_USERS.append(user)
        dt = getDatetime()

        # build embed
        embed = Embed(title="Your reported prices",
                      description="Select the one you would like to edit using reactions.\n❌ to cancel editing.",
                      color=0xFFAB40,
                      timestamp=dt)
        embed.set_thumbnail(url=THUMB_NORMAL)
        embed.set_footer(text="Reported prices of {0.display_name} ({1.ingameName})".format(user, person), icon_url=str(user.avatar_url))
        
        daymap = {
            '0️⃣': "Sun. Morning",
            '1️⃣': "Mon. Morning",
            '2️⃣': "Mon. Afternoon",
            '3️⃣': "Tue. Morning",
            '4️⃣': "Tue. Afternoon",
            '5️⃣': "Wed. Morning",
            '6️⃣': "Wed. Afternoon",
            '7️⃣': "Thu. Morning",
            '8️⃣': "Thu. Afternoon",
            '9️⃣': "Fri. Morning",
            '🇦': "Fri. Afternoon",
            '🇧': "Sat. Morning",
            '🇨': "Sat. Afternoon",
        }

        def makeHandler(date, morning: bool):
            async def handler(rm: ReactableMessage, reaction: Reaction, user: User):
                rm.stop()
                LOG.debug("got reaction ({1}) from {0.name} ({0.id})".format(user, reaction))

                channel = reaction.message.channel
                # send instructions message
                await channel.send("What price did you have on {0:%A} {1}?".format(date, "Morning" if morning else "Afternoon"))

                def check(m):
                    try:
                        return m.channel == channel and m.author == user and int(m.content) > 0 and int(m.content) < 999
                    except ValueError:
                        LOG.debug("message check failed because of invalid price")
                        return False

                # wait for answer
                try:
                    msg = await bot.wait_for('message', timeout=60.0, check=check)
                except TimeoutError:
                    LOG.warning("user {0.name} ({0.id}) did not deliver an updated turnip price in time".format(user))
                    await channel.send("Timed out while waiting for the updated price.")
                    return
                
                LOG.debug("updating turnip price for {0.name} ({0.id}) on {1:%A} {2}".format(user, date, "Morning" if morning else "Afternoon"))
                price = int(msg.content)
                person = getPerson(user.id)
                # update turnip price
                setTurnipPrice(price, person, date, morning)
                # unlock user
                try:
                    EDITING_USERS.remove(user)
                except ValueError:
                    LOG.debug("user {0.name} ({0.id}) not found in EDITING_USERS ???".format(user))

                await channel.send("Updated your {0:%A} {1} price to {2}⭐!".format(date, "Morning" if morning else "Afternoon", price))

            return handler

        # make handlers for all reactions and add embed fields
        sunday, _ = getStartEndOfWeek(dt)
        handlers = {
            '0️⃣': makeHandler(sunday, True)
        }

        # add sunday field
        price = getTurnipPrice(person, sunday, True)
        embed.add_field(name="{0}: {1}".format('0️⃣', 'Sunday Morning (Buy Price)'), value=price.turnipPrice if price is not None else '❓', inline=False)

        date = sunday + timedelta(days=1)
        if not isSunday(dt):
            for k in list(daymap.keys())[1:]:
                morning = daymap[k].endswith("Morning")
                
                # add embed field
                price = getTurnipPrice(person, date, morning)
                embed.add_field(name="{0}: {1}".format(k, daymap[k]), value=price.turnipPrice if price is not None else '❓', inline=True)

                # add handler
                handlers[k] = makeHandler(date, morning)
                
                # stop adding handlers and fields when current turnip time is reached
                if date == dt.date() and morning == isMorning(dt):
                    break

                # advance date
                if daymap[k].endswith("Afternoon"):
                    date += timedelta(days=1)
                
                if date > dt.date():
                    break

        async def onTimeout():
            try:
                EDITING_USERS.remove(user)
            except ValueError:
                pass

        # add cancel handler
        async def onCancel(rm: ReactableMessage, reaction: Reaction, user: User):
            rm.stop()
            
            try:
                await rm.message.edit(content="Canceled editing.", embed=None)
            except (Forbidden, HTTPException, NotFound):
                pass

            try:
                EDITING_USERS.remove(user)
            except ValueError:
                pass

        handlers['❌'] = onCancel

        # send message
        msg = await ctx.channel.send(embed=embed)
        rm = ReactableMessage(bot, msg, limitedTo=[ctx.message.author.id], reactionHandlers=handlers, onTimeout=onTimeout)
        # start listener
        await rm.start()

    @turnips_edit.error
    async def turnip_edit_error(ctx, error):
        LOG.critical("unknown error occured while running edit command for {0.name} ({0.id}): {1}".format(ctx.message.author, error))

    @turnips.command(name='register')
    @dm_only()
    async def turnips_register(ctx, ingameName: str, islandName: str, fruit: str):
        """Register your ingame information to be able to use this bot."""

        LOG.debug("register command issued by {}".format(ctx.message.author.name))    
        
        if len(fruit) > 1 or fruit not in FRUIT:
            await ctx.send("Invalid fruit type! Only " + FRUIT + " are allowed.")
            return

        # create the person
        person = getPerson(ctx.message.author.id)
        person.nativeFruit = fruit
        person.ingameName = ingameName
        person.islandName = islandName
        person.save()

        await ctx.send("Successfully saved your info!")

    @turnips_register.error
    async def turnips_register_error(ctx, error):
        if isinstance(error, PrivateMessageOnly):
            LOG.warning("{} tried to use a DM only command in a guild.".format(ctx.message.author))
            try:
                await ctx.message.delete()
            except Forbidden:
                LOG.error("Failed to delete message#{0.message.id} in channel#{0.channel.id} in guild#{0.guild.id}, missing permissions!".format(ctx))
            except NotFound:
                LOG.warning("Could not find message#{} to delete".format(ctx.message.id))
            except HTTPException as e:
                LOG.critical("Some HTTP Exception occured while trying to delete message#{}: {}".format(ctx.message.id, e))

            await ctx.message.author.send("The command you tried to use only works in DMs!")

    @turnips.command(name='init')
    @guild_only()
    @has_permissions(administrator=True)
    async def turnips_init(ctx):
        """Setup turnip tracker message in a channel."""
        turnipMsg = getMessage(ctx.guild.id, ctx.channel.id)

        if turnipMsg is not None:
            await ctx.send("This channel already has a stalk market tracker!")
            return

        # send a stalk market message
        await sendMessage(ctx.channel)

    @turnips.command(name='remove')
    @guild_only()
    @has_permissions(administrator=True)
    async def turnips_remove(ctx):
        """Remove the turnip tracker from a channel."""
        turnipMsg = getMessage(ctx.guild.id, ctx.channel.id)

        if turnipMsg is None:
            await ctx.send("This channel does not have a stalk market tracker!")
            return

        # get the message
        msg = None
        try:
            msg = await ctx.channel.fetch_message(turnipMsg.messageId)
            LOG.debug("found message")
        except Forbidden:
            LOG.error("Missing read permission on guild#{0.guild.id} '{0.guild.name}' in channel#{0.channel.id} '{0.channel.name}!'".format(ctx))
        except NotFound:
            LOG.critical("Message on guild#{0.guild.id} '{0.guild.name}' in channel#{0.channel.id} '{0.channel.name} was deleted!'".format(ctx))
        except HTTPException:
            pass

        # unpin the message
        if msg is not None:
            try:
                await msg.unpin()
            except Forbidden:
                LOG.error("Missing pinning permission on guild#{0.guild.id} '{0.guild.name}' in channel#{0.channel.id} '{0.channel.name}!'".format(ctx))
                await ctx.send("Could not unpin the message because of missing permission, please unpin it yourself!")
            except NotFound:
                LOG.critical("Message on guild#{0.guild.id} '{0.guild.name}' in channel#{0.channel.id} '{0.channel.name} was deleted!'".format(ctx))
            except HTTPException as e:
                LOG.critical("Failed to unpin message: " + e)

        # delete the message entry
        turnipMsg.delete_instance()
        await ctx.send("This channel will no longer track the stalk market!")

    @bot.listen()
    async def on_message(msg: Message):
        """Get updated turnip price from DMs."""
        # ignore own messages
        if msg.author == bot.user:
            return

        # ignore messages of users that are currently editing
        if msg.author in EDITING_USERS:
            return

        # ignore commands
        if msg.content.startswith(CONF["cmd_prefix"]):
            return

        # only check if DM
        if not isinstance(msg.channel, DMChannel):
            LOG.debug("message from {} not in a DM".format(msg.author.name))
            return

        # only update if person has set up an account
        if not hasPerson(msg.author.id):
            LOG.debug("{} does not have animal crossing info set up yet".format(msg.author.name))
            await msg.channel.send("You are not registered yet! Please use `{0}turnips register \"Ingame Name\" \"Island Name\" 🍎/🍐/🍑/🍒/🍊`".format(CONF["cmd_prefix"]))
            return

        # check if the market is open
        dt = getDatetime()
        if isMarketClosed(dt):
            await msg.channel.send("Sorry, the stalk market is closed right now. The stalk market opens at 8AM and closes at 10PM and on sundays only from 8AM to 12PM!")
            return

        # try to get the price amount
        try:
            price = int(msg.content)
        except (ValueError, TypeError):
            LOG.debug("failed to get price from message '{}' by {}".format(msg.content, msg.author.name))
            return

        if price < 1 or price > 999:
            await msg.channel.send("Invalid turnip price!")
            return
        
        # update the turnip price of the person
        LOG.debug("updating turnip price to of {} to {}".format(msg.author.name, price))
        person = getPerson(msg.author.id)
        setTurnipPrice(price, person, date=dt.date(), morning=isMorning(dt))

        # send confirmation
        await msg.channel.send("Setting your turnip price to {}⭐!".format(price))

        # get all guilds this user is on
        guilds = [g.id for g in bot.guilds if g.get_member(msg.author.id) is not None]
        # update the guilds
        if guilds:
            LOG.debug("updating guild messages: " + str(guilds))
            await updateMessages(bot, guilds, False)
        
        ## check, if the user has a surefire prediction on the hand
        # if current pattern is expired, move it to last pattern
        if person.pattern_valid_until < dt.date():
            new_validity = getStartOfNextWeek(dt) - timedelta(days=1)
            # don't save it in last pattern if the pattern is not the one from last week
            if person.pattern_valid_until < new_validity - timedelta(weeks=1):
                person.last_pattern = -1
            else:
                person.last_pattern = person.current_pattern
            person.current_pattern = -1
            person.pattern_valid_until = new_validity
            person.save()

        # don't do predictions on sunday
        if isSunday(dt):
            return
        
        sunday, _ = getStartEndOfWeek()
        try:
            TurnipPrice.get((TurnipPrice.person == person) & (TurnipPrice.date == sunday) & (TurnipPrice.morning == True))
        except DoesNotExist:
            LOG.warning("Missing sunday turnip price for {0.name} ({0.id})".format(msg.author))
            return

        # run pattern analysis asyncly
        async def checkPredictions(msg: Message):
            LOG.debug("running surefire predictions for user {0.name} ({0.id})".format(msg.author))
            try:
                generated_possibilities, all_patterns = get_prediction_for_person(person)
            except (InvalidPrices, NoPricesRegistered) as e:
                if isinstance(e, InvalidPrices):
                    LOG.warning("invalid prices for {0.name} ({0.id}) found while analyzing predictions".format(msg.author))
                return
            
            prediction = get_surefire_prediction(generated_possibilities)

            if prediction and person.current_pattern == -1:
                LOG.debug("found surefire prediction for {0.name} ({0.id})".format(msg.author))
                idx_min = get_index_of_min_prediction(prediction)
                idx_max = get_index_of_max_prediction(prediction)
                week = ["Sunday morning", "Sunday afternoon", 
                "Monday morning", "Monday afternoon", 
                "Tuesday morning", "Tuesday afternoon", 
                "Wednesday morning", "Wednesday afternoon", 
                "Thursday morning", "Thursday afternoon", 
                "Friday morning", "Friday afternoon", 
                "Saturday morning", "Saturday afternoon"]
                person.current_pattern = prediction['pattern_number']
                person.pattern_valid_until = getStartOfNextWeek(dt) - timedelta(days=1)
                person.save()
                await msg.channel.send(f"After analyzing price patterns, you will get a minimum of **{prediction['week_guaranteed_minimum']}** bells per turnip on **{week[idx_min]}** for the rest of this week (current market cycle not included). Additionally, you have a chance of netting **up to {prediction['week_max']}** bells per turnip on **{week[idx_max]}** (current market cycle not included).")
            LOG.debug("done analyzing predictions for {0.name} ({0.id})".format(msg.author))

        schedule.now(bot, checkPredictions, args=[msg])

    @bot.listen()
    async def on_ready():
        """Schedule message edits"""
        # update messages and schedule next update
        await updateMessages(bot, [], False)
        # update presence
        if SCHEDULED_PRESENCE is None:
            await updatePresence(bot)

    


        

        
