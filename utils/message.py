from discord import Message, HTTPException, Forbidden, NotFound, User, Reaction, Emoji, PartialEmoji

from . import schedule

class ReactableMessage:

    def __init__(self, bot, msg: Message, timeout: int = 600, limitedTo: list = [], reactionHandlers: dict = {}, onTimeout = None):
        super().__init__()
        self.bot = bot
        self.message = msg
        self._limitedTo = limitedTo
        self._reactionHandlers = reactionHandlers
        self._timeout = timeout
        self._timeoutId = None
        self._onTimeout = onTimeout

        self._started = False
        self._done = False

    def isStarted(self) -> bool:
        """If the message is currently listening for reactions."""
        return self._started

    def isDone(self) -> bool:
        """If the message is done listening for reactions."""
        return self._done

    async def start(self):
        """Start listening for reactions."""
        self._started = True

        # register listener
        self.bot.add_listener(self.onReaction, 'on_reaction_add')

        # schedule timeout
        self.scheduleTimeout()

        # add reactions
        for r in self._reactionHandlers.keys():
            e = None
            if isinstance(r, str):
                e = r
            else:
                e = PartialEmoji(id=r)
            
            try:
                await self.message.add_reaction(e)
            except (HTTPException, Forbidden, NotFound):
                pass

    def scheduleTimeout(self, timeout: int = None):
        """(Re-)Schedule the timout handler."""
        if timeout is None:
            timeout = self._timeout

        # cancel old timeout
        if self._timeoutId is not None:
            schedule.cancel(self._timeoutId)

        # schedule new timeout
        self._timeoutId = schedule.add(self.bot, self.timeout, timeout, absolute=False, args=[self.bot])

    def stop(self):
        """Stop listening for reactions on this message."""
        if not self.isStarted() or self.isDone():
            return

        self._started = False
        self._done = True

        # disable reaction handler
        self.bot.remove_listener(self.onReaction, 'on_reaction_add')

        # cancel timeout
        if self._timeoutId:
            schedule.cancel(self._timeoutId)
    
    async def onReaction(self, reaction: Reaction, user: User):
        """Main reaction listener triggering the specific handlers."""
        if user == self.bot.user:
            return

        # only accept certain users
        if self._limitedTo and user.id not in self._limitedTo:
            try:
                await reaction.remove(user)
            except (HTTPException, Forbidden, NotFound):
                pass
            return

        emoji = None
        # use ids of custom emoji
        if isinstance(reaction.emoji, (Emoji, PartialEmoji)):
            emoji = reaction.emoji.id
        else:
            emoji = reaction.emoji

        # run the handler if exists
        try:
            await self._reactionHandlers[emoji](self, reaction, user)
            # reschedule timeout after handling reaction
            self.scheduleTimeout()
        except KeyError:
            # try to remove invalid reaction
            try:
                await reaction.remove(user)
            except(HTTPException, Forbidden, NotFound):
                return

    async def timeout(self, bot):
        """Timeout coroutine to stop all listeners for this message if nothing happens."""
        # message was already stopped, no timeout needed
        if self.isDone():
            return

        if self._onTimeout is not None:
            await self._onTimeout()

        # disable reaction handler
        self.stop()

        # edit message
        try:
            await self.message.edit(content="**Timed out waiting for a reaction.**", embed=None)
        except (HTTPException, Forbidden, NotFound):
            pass

    def registerReaction(self, reaction, handler):
        """Add a reaction (unicode str or int id) to be handled."""
        self._reactionHandlers[reaction] = handler

    def removeReaction(self, reaction):
        """Delete a reaction from triggering a function."""
        try:
            del self._reactionHandlers[reaction]
        except KeyError:
            pass