import math
from functools import reduce, cmp_to_key

# ported from: https://github.com/mikebryant/ac-nh-turnip-prices/blob/master/js/predictions.js

class Pattern:
    FLUCTUATING = 0
    LARGE_SPIKE = 1
    DECREASING = 2
    SMALL_SPIKE = 3

class InvalidPrices(Exception):
    pass

PROBABILITY_MATRIX = {
    Pattern.FLUCTUATING: {
       Pattern.FLUCTUATING: 0.2,
       Pattern.LARGE_SPIKE: 0.3,
       Pattern.DECREASING: 0.15,
       Pattern.SMALL_SPIKE: 0.35
    },
    Pattern.LARGE_SPIKE: {
       Pattern.FLUCTUATING: 0.5,
       Pattern.LARGE_SPIKE: 0.05,
       Pattern.DECREASING: 0.2,
       Pattern.SMALL_SPIKE: 0.25
    },
    Pattern.DECREASING: {
       Pattern.FLUCTUATING: 0.25,
       Pattern.LARGE_SPIKE: 0.45,
       Pattern.DECREASING: 0.05,
       Pattern.SMALL_SPIKE: 0.25
    },
    Pattern.SMALL_SPIKE: {
       Pattern.FLUCTUATING: 0.45,
       Pattern.LARGE_SPIKE: 0.25,
       Pattern.DECREASING: 0.15,
       Pattern.SMALL_SPIKE: 0.15
    }
}

RATE_MULTIPLIER = 10000

def clamp(x, _min, _max):
    return min(max(x, _min), _max)

def range_length(_range):
    return _range[1] - _range[0]

def range_intersect(range1, range2):
    if range1[0] > range2[1] or range1[1] < range2[0]:
        return None
    return [max(range1[0], range2[0]), min(range1[1], range2[1])]

def range_intersect_length(range1, range2):
    if range1[0] > range2[1] or range1[1] < range2[0]:
        return 0
    return range_length(range_intersect(range1, range2))

class PDF:

    def __init__(self, a, b, uniform = True):
        self.value_start = round(a)
        self.value_end = round(b)

        _range = [a, b]
        total_length = range_length(_range)
        self.prob = [None] * (self.value_end - self.value_start + 1)
        if uniform:
            for i in range(0, len(self.prob)):
                self.prob[i] = range_intersect_length(self.range_of(i), _range) / total_length
    
    def range_of(self, idx):
        return [self.value_start + idx - 0.5, self.value_start + idx + 0.5 - 1e-9]
    
    def min_value(self):
        return self.value_start - 0.5
    
    def max_value(self):
        return self.value_end + 0.5 - 1e-9
    
    def normalize(self):
        total_probability = reduce((lambda acc, it: acc + it), self.prob, 0)
        for i in range(0, len(self.prob)):
            self.prob[i] = self.prob[i] / total_probability
    
    def range_limit(self, _range):
        [start, end] = _range
        start = max(start, self.min_value())
        end = min(end, self.max_value())
        if start >= end:
            self.value_start = self.value_end = 0
            self.prob = []
            return 0
        
        prob = 0
        start_idx = round(start) - self.value_start
        end_idx = round(end) - self.value_start
        for i in range(start_idx, end_idx+1):
            bucket_prob = self.prob[i] * range_intersect_length(self.range_of(i), _range)
            self.prob[i] = bucket_prob
            prob = prob + bucket_prob
        
        self.prob = self.prob[start_idx:end_idx+1]
        self.value_start = round(start)
        self.value_end = round(end)
        self.normalize()

        return prob
    
    def decay(self, rate_decay_min, rate_decay_max):
        ret = PDF(self.min_value() - rate_decay_max, self.max_value() - rate_decay_min, False)
        for i in range(1, len(self.prob)):
            self.prob[i] = self.prob[i] + self.prob[i - 1]
        
        def _sum(l, r):
            l -= self.value_start
            r -= self.value_start
            if l < 0: l = 0
            if r > len(self.prob): r = len(self.prob)
            if l >= r: return 0
            l = int(l)
            r = int(r)
            return self.prob[r - 1] - (0 if l == 0 else self.prob[l - 1])
        
        for x in range(0, len(ret.prob)):
            ret.prob[x] = _sum(x + rate_decay_min + 1 + ret.value_start, x + rate_decay_max + 1 + ret.value_start)
            ret.prob[x] = ret.prob[x] + _sum(x + rate_decay_min + ret.value_start, x + rate_decay_max + ret.value_start)
        
        self.prob = ret.prob
        self.value_start = ret.value_start
        self.value_end = ret.value_end
        self.normalize()

class Predictor:

    def __init__(self, prices, first_buy, previous_pattern):
        self.fudge_factor = 0
        self.prices = prices
        self.first_buy = first_buy
        self.previous_pattern = previous_pattern
    
    def intceil(self, val):
        return math.trunc(val + 0.99999)
    
    def minimum_rate_from_given_and_base(self, given_price, buy_price):
        return RATE_MULTIPLIER * (given_price - 0.99999) / buy_price

    def maximum_rate_from_given_and_base(self, given_price, buy_price):
        return RATE_MULTIPLIER * (given_price + 0.00001) / buy_price

    def rate_range_from_given_and_base(self, given_price, buy_price):
        return [self.minimum_rate_from_given_and_base(given_price, buy_price), self.maximum_rate_from_given_and_base(given_price, buy_price)]

    def get_price(self, rate, base_price):
        return self.intceil(rate * base_price / RATE_MULTIPLIER)

    def multiply_generator_probability(self, generator, probability):
        for it in generator:
            yield {**it, 'probability': it['probability'] * probability}
    
    def generate_individual_random_price(self, given_prices, predicted_prices, start, length, rate_min, rate_max):
        rate_min = rate_min * RATE_MULTIPLIER
        rate_max = rate_max * RATE_MULTIPLIER

        buy_price = given_prices[0]
        rate_range = [rate_min, rate_max]
        prob = 1

        for i in range(start, start + length):
            min_pred = self.get_price(rate_min, buy_price)
            max_pred = self.get_price(rate_max, buy_price)

            if given_prices[i] is not None and not math.isnan(given_prices[i]):
                if given_prices[i] < min_pred - self.fudge_factor or given_prices[i] > max_pred + self.fudge_factor:
                    return 0

                real_rate_range = self.rate_range_from_given_and_base(clamp(given_prices[i], min_pred, max_pred), buy_price)
                prob *= range_intersect_length(rate_range, real_rate_range) / range_length(rate_range)
                min_pred = given_prices[i]
                max_pred = given_prices[i]

            predicted_prices.append({
                'min': min_pred,
                'max': max_pred
            })

        return prob

    def generate_decreasing_random_price(self, given_prices, predicted_prices, start, length, start_rate_min, start_rate_max, rate_decay_min, rate_decay_max):
        start_rate_min *= RATE_MULTIPLIER
        start_rate_max *= RATE_MULTIPLIER
        rate_decay_min *= RATE_MULTIPLIER
        rate_decay_max *= RATE_MULTIPLIER

        buy_price = given_prices[0]
        rate_pdf = PDF(start_rate_min, start_rate_max)
        prob = 1

        for i in range(start, start + length):
            min_pred = self.get_price(rate_pdf.min_value(), buy_price)
            max_pred = self.get_price(rate_pdf.max_value(), buy_price)
            if given_prices[i] is not None and not math.isnan(given_prices[i]):
                if given_prices[i] < min_pred - self.fudge_factor or given_prices[i] > max_pred + self.fudge_factor:
                    return 0

                real_rate_range = self.rate_range_from_given_and_base(clamp(given_prices[i], min_pred, max_pred), buy_price)
                prob *= rate_pdf.range_limit(real_rate_range)
                if prob == 0: return 0
                min_pred = given_prices[i]
                max_pred = given_prices[i]

            predicted_prices.append({
                'min': min_pred,
                'max': max_pred
            })
            rate_pdf.decay(rate_decay_min, rate_decay_max)
        return prob

    def generate_peak_price(self, given_prices, predicted_prices, start, rate_min, rate_max):
        rate_min *= RATE_MULTIPLIER
        rate_max *= RATE_MULTIPLIER

        buy_price = given_prices[0]
        prob = 1
        rate_range = [rate_min, rate_max]

        middle_price = given_prices[start + 1]
        if middle_price is not None and not math.isnan(middle_price):
            min_pred = self.get_price(rate_min, buy_price)
            max_pred = self.get_price(rate_max, buy_price)

            if middle_price < min_pred - self.fudge_factor or middle_price > max_pred + self.fudge_factor:
                return 0

            real_rate_range = self.rate_range_from_given_and_base(clamp(middle_price, min_pred, max_pred), buy_price)
            prob *= range_intersect_length(rate_range, real_rate_range) / range_length(rate_range)
            if prob == 0: return 0

            rate_range = range_intersect(rate_range, real_rate_range)

        left_price = given_prices[start]
        right_price = given_prices[start + 2]

        for price in [left_price, right_price]:
            if math.isnan(price): continue
            min_pred = self.get_price(rate_min, buy_price) - 1
            max_pred = self.get_price(rate_range[1], buy_price) - 1
            if price < min_pred - self.fudge_factor or price > max_pred + self.fudge_factor:
                return 0

            rate2_range = self.rate_range_from_given_and_base(clamp(price, min_pred, max_pred) + 1, buy_price)
            def F(t, ZZ):
                if t <= 0: return 0
                return ZZ if ZZ < t else t - t * (math.log(t) - math.log(ZZ))
            [A, B] = rate_range
            C = rate_min
            Z1 = A - C
            Z2 = B - C
            PY = (lambda t: (F(t - C, Z2) - F(t - C, Z1)) / (Z2 - Z1))
            prob *= PY(rate2_range[1]) - PY(rate2_range[0])
            if prob == 0: return 0

        min_pred = self.get_price(rate_min, buy_price) - 1
        max_pred = self.get_price(rate_max, buy_price) - 1

        if given_prices[start] is not None and not math.isnan(given_prices[start]):
            min_pred = given_prices[start]
            max_pred = given_prices[start]

        predicted_prices.append({
            'min': min_pred,
            'max': max_pred
        })

        min_pred = predicted_prices[start]['min']
        max_pred = self.get_price(rate_max, buy_price)
        if given_prices[start + 1] is not None and not math.isnan(given_prices[start + 1]):
            min_pred = given_prices[start + 1]
            max_pred = given_prices[start + 1]

        predicted_prices.append({
            'min': min_pred,
            'max': max_pred
        })

        min_pred = self.get_price(rate_min, buy_price) - 1
        max_pred = predicted_prices[start + 1]['max'] - 1
        if given_prices[start + 2] is not None and not math.isnan(given_prices[start + 2]):
            min_pred = given_prices[start + 2]
            max_pred = given_prices[start + 2]

        predicted_prices.append({
            'min': min_pred,
            'max': max_pred,
        })

        return prob

    def generate_pattern_0_with_lengths(self, given_prices, high_phase_1_len, dec_phase_1_len, high_phase_2_len, dec_phase_2_len, high_phase_3_len):
        buy_price = given_prices[0]
        predicted_prices = [
            {
                'min': buy_price,
                'max': buy_price
            },
            {
                'min': buy_price,
                'max': buy_price
            }
        ]
        probability = 1

        probability *= self.generate_individual_random_price(given_prices, predicted_prices, 2, high_phase_1_len, 0.9, 1.4)
        if probability == 0: return

        probability *= self.generate_decreasing_random_price(given_prices, predicted_prices, 2 + high_phase_1_len, dec_phase_1_len, 0.6, 0.8, 0.04, 0.1)
        if probability == 0: return

        probability *= self.generate_individual_random_price(given_prices, predicted_prices, 2 + high_phase_1_len + dec_phase_1_len, high_phase_2_len, 0.9, 1.4)
        if probability == 0: return

        probability *= self.generate_decreasing_random_price(given_prices, predicted_prices, 2 + high_phase_1_len + dec_phase_1_len + high_phase_2_len, dec_phase_2_len, 0.6, 0.8, 0.04, 0.1)
        if probability == 0: return

        if 2 + high_phase_1_len + dec_phase_1_len + high_phase_2_len + dec_phase_2_len + high_phase_3_len != 14:
            raise Exception("Phase lengths don't add up")

        prev_length = 2 + high_phase_1_len + dec_phase_1_len + high_phase_2_len + dec_phase_2_len
        probability *= self.generate_individual_random_price(given_prices, predicted_prices, prev_length, 14 - prev_length, 0.9, 1.4)
        if probability == 0: return

        yield {
            'pattern_number': 0,
            'prices': predicted_prices,
            'probability': probability
        }

    def generate_pattern_0(self, given_prices):
        for dec_phase_1_len in range(2, 4):
            for high_phase_1_len in range(0, 7):
                for high_phase_3_len in range(0, (7 - high_phase_1_len - 1 + 1)):
                    yield from self.multiply_generator_probability(self.generate_pattern_0_with_lengths(given_prices, high_phase_1_len, dec_phase_1_len, 7 - high_phase_1_len - high_phase_3_len, 5 - dec_phase_1_len, high_phase_3_len), 1 / (4 - 2) / 7 / (7 - high_phase_1_len))

    def generate_pattern_1_with_peak(self, given_prices, peak_start):
        buy_price = given_prices[0]
        predicted_prices = [
            {
                'min': buy_price,
                'max': buy_price
            },
            {
                'min': buy_price,
                'max': buy_price
            }
        ]
        probability = 1

        probability *= self.generate_decreasing_random_price(given_prices, predicted_prices, 2, peak_start - 2, 0.85, 0.9, 0.03, 0.05)
        if probability == 0: return

        min_randoms = [0.9, 1.4, 2.0, 1.4, 0.9, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4]
        max_randoms = [1.4, 2.0, 6.0, 2.0, 1.4, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9]
        for i in range(peak_start, 14):
            probability *= self.generate_individual_random_price(given_prices, predicted_prices, i, 1, min_randoms[i - peak_start], max_randoms[i - peak_start])
            if probability == 0: return

        yield {
            'pattern_number': 1,
            'prices': predicted_prices,
            'probability': probability
        }

    def generate_pattern_1(self, given_prices):
        for peak_start in range(3, 10):
            yield from self.multiply_generator_probability(self.generate_pattern_1_with_peak(given_prices, peak_start), 1 / (10 - 3))

    def generate_pattern_2(self, given_prices):
        buy_price = given_prices[0]
        predicted_prices = [
            {
                'min': buy_price,
                'max': buy_price
            },
            {
                'min': buy_price,
                'max': buy_price
            }
        ]
        probability = 1
        probability *= self.generate_decreasing_random_price(given_prices, predicted_prices, 2, 14 - 2, 0.85, 0.9, 0.03, 0.05)
        if probability == 0: return

        yield {
            'pattern_number': 2,
            'prices': predicted_prices,
            'probability': probability
        }

    def generate_pattern_3_with_peak(self, given_prices, peak_start):
        buy_price = given_prices[0]
        predicted_prices = [
            {
                'min': buy_price,
                'max': buy_price
            },
            {
                'min': buy_price,
                'max': buy_price
            }
        ]
        probability = 1

        probability *= self.generate_decreasing_random_price(given_prices, predicted_prices, 2, peak_start - 2, 0.4, 0.9, 0.03, 0.05)
        if probability == 0: return

        probability *= self.generate_individual_random_price(given_prices, predicted_prices, peak_start, 2, 0.9, 1.4)
        if probability == 0: return

        probability *= self.generate_peak_price(given_prices, predicted_prices, peak_start + 2, 1.4, 2.0)
        if probability == 0: return

        if peak_start + 5 < 14:
            probability *= self.generate_decreasing_random_price(given_prices, predicted_prices, peak_start + 5, 14 - (peak_start + 5), 0.4, 0.9, 0.03, 0.05)
            if probability == 0: return

        yield {
            'pattern_number': 3,
            'prices': predicted_prices,
            'probability': probability
        }

    def generate_pattern_3(self, given_prices):
        for peak_start in range(2, 10):
            yield from self.multiply_generator_probability(self.generate_pattern_3_with_peak(given_prices, peak_start), 1 / (10 - 2))

    def get_transition_probability(self, previous_pattern):
        if not previous_pattern or previous_pattern < 0 or previous_pattern > 3:
            return [4530/13082, 3236/13082, 1931/13082, 3385/13082]
        return PROBABILITY_MATRIX[previous_pattern]

    def generate_all_patterns(self, sell_prices, previous_pattern):
        generate_pattern_fns = [self.generate_pattern_0, self.generate_pattern_1, self.generate_pattern_2, self.generate_pattern_3]
        transition_probability = self.get_transition_probability(previous_pattern)

        for i in range(0, 4):
            yield from self.multiply_generator_probability(generate_pattern_fns[i](sell_prices), transition_probability[i])

    def generate_possibilities(self, sell_prices, first_buy, previous_pattern):
        temp_sell_prices = list(sell_prices)
        if first_buy or math.isnan(sell_prices[0]):
            for buy_price in range(90, 111):
                temp_sell_prices[0] = temp_sell_prices[1] = buy_price
                if first_buy:
                    yield from self.generate_pattern_3(temp_sell_prices)
                else:
                    yield from self.generate_all_patterns(temp_sell_prices, previous_pattern)
        else:
            yield from self.generate_all_patterns(temp_sell_prices, previous_pattern)

    def analyze_possibilities(self):
        sell_prices = self.prices
        first_buy = self.first_buy
        previous_pattern = self.previous_pattern
        generated_possibilities = []
        for i in range(0, 6):
            self.fudge_factor = i
            generated_possibilities = list(self.generate_possibilities(sell_prices, first_buy, previous_pattern))
            if generated_possibilities:
                break
        #generated_possibilities = generate_possibilities(sell_prices, first_buy, previous_pattern)
        #print(generated_possibilities)

        global_pattern = 4
        tmp_pattern = generated_possibilities[0]['pattern_number'] if generated_possibilities else 4 # get first pattern
        total_probability = reduce((lambda acc, it: acc + it['probability']), generated_possibilities, 0)
        for it in generated_possibilities:
            if tmp_pattern != it['pattern_number']: # if any other pattern was generated, no global pattern exists
                tmp_pattern = 4
            it['probability'] /= total_probability

        global_pattern = tmp_pattern

        for poss in generated_possibilities:
            weekMins = []
            weekMaxes = []
            for day in poss['prices'][2:len(poss['prices'])]:
                if day['min'] != day['max']:
                    weekMins.append(day['min'])
                    weekMaxes.append(day['max'])
                else:
                    weekMins = []
                    weekMaxes = []
            if not weekMins and not weekMaxes:
                weekMins.append(poss['prices'][len(poss['prices'])-1]['min'])
                weekMaxes.append(poss['prices'][len(poss['prices'])-1]['max'])

            poss['week_guaranteed_minimum'] = max(*weekMins)
            poss['week_max'] = max(*weekMaxes)
        
        category_totals = {}
        for i in range(0, 4):
            category_totals[i] = reduce((lambda previous, current: previous + current), map(lambda v: v['probability'], filter(lambda v: v['pattern_number'] == i, generated_possibilities)), 0)
        
        for pos in generated_possibilities:
            pos['category_total_probability'] = category_totals[pos['pattern_number']]

        def _cmp(a, b):
            return b['category_total_probability'] - a['category_total_probability'] or b['probability'] - a['probability']
        generated_possibilities = sorted(generated_possibilities, key=cmp_to_key(_cmp))

        global_min_max = []
        for day in range(0, 14):
            prices = {
                'min': 999,
                'max': 0
            }

            for poss in generated_possibilities:
                if poss['prices'][day]['min'] < prices['min']:
                    prices['min'] = poss['prices'][day]['min']
                if poss['prices'][day]['max'] > prices['max']:
                    prices['max'] = poss['prices'][day]['max']
            global_min_max.append(prices)

        try:
            all_patterns = {
                'pattern_number': global_pattern,
                'prices': global_min_max,
                'week_guaranteed_minimum': min(map((lambda poss: poss['week_guaranteed_minimum']), generated_possibilities)),
                'week_max': max(map((lambda poss: poss['week_max']), generated_possibilities))
            }
        except ValueError:
            raise InvalidPrices()

        #generated_possibilities.insert(0, all_patterns)

        return generated_possibilities, all_patterns

def get_closest_prediction(generated_possibilities, incl_max_prob=1):
    # Ignore the first element, which is always the "all patterns" element and which doesn't have a probability
    sorted_possibilities = sorted(generated_possibilities, key=(lambda poss: poss['probability']), reverse=True)
    if sorted_possibilities[0]['probability'] >= incl_max_prob:
        return sorted_possibilities[0]
    return None

def get_surefire_prediction(generated_possibilities):
    """Returns a prediction, that has a 100% probability of happening, else it returns nothing"""
    return get_closest_prediction(generated_possibilities, 1)

def get_index_of_max_prediction(prediction):
    highest = prediction['week_max']
    for idx, price in enumerate(prediction['prices']):
        if price['max'] == highest: return idx
    return -1

def get_index_of_min_prediction(prediction):
    lowest = prediction['week_guaranteed_minimum']
    for idx, price in enumerate(prediction['prices']):
        if price['min'] == lowest: return idx
    return -1